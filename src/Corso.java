import java.util.ArrayList;
import java.util.List;

public class Corso {
    private String titoloCorso;
    private int codiceCors;
    private String professoreTitolare;
    private List<Studente> studentiIscritti;
    private int maxStudenti;



    public String getTitoloCorso() {
        return titoloCorso;
    }

    public void setTitoloCorso(String titoloCorso) {
        this.titoloCorso = titoloCorso;
    }

    public int getCodiceCors() {
        return codiceCors;
    }

    public void setCodiceCors(int codiceCors) {
        this.codiceCors = codiceCors;
    }

    public String getProfessoreTitolare() {
        return professoreTitolare;
    }

    public void setProfessoreTitolare(String professoreTitolare) {
        this.professoreTitolare = professoreTitolare;
    }

    public List<Studente> getStudentiIscritti() {
        return studentiIscritti;
    }

    public void setStudentiIscritti(List<Studente> studentiIscritti) {
        this.studentiIscritti = studentiIscritti;
    }

    public int getMaxStudenti() {
        return maxStudenti;
    }

    public void setMaxStudenti(int maxStudenti) {
        this.maxStudenti = maxStudenti;
    }

    //Costruttore
    public Corso(String titoloCorso, String professoreTitolare, int codiceCors,int maxStudenti ){
        this.titoloCorso=titoloCorso;
        this.professoreTitolare=professoreTitolare;
        this.codiceCors=codiceCors;
        this.maxStudenti=maxStudenti;
        List<Studente> studentiIscritti =new ArrayList<>();
        this.studentiIscritti=studentiIscritti;
    }
    //aggiunta e rimozione di studenti
    public void aggiuntaStudente(Studente studente){
        studentiIscritti.add(studente);
    }
    public void rimozioneStudente(Studente studente){
        int indice=studentiIscritti.indexOf(studente);
        studentiIscritti.remove(indice);
    }

    //Assegnazione di un professore al corso
    public void assegnaProf(String professoreTitolare){
        this.professoreTitolare=professoreTitolare;
    }



}
