import java.util.ArrayList;
import java.util.List;

public class Studente {
    private String nome;
    private String cognome;
    private int numeroMatricola;
    private int annoImmatricolazione;
    private List<String> listaCorsi;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public int getNumeroMatricola() {
        return numeroMatricola;
    }

    public void setNumeroMatricola(int numeroMatricola) {
        this.numeroMatricola = numeroMatricola;
    }

    public int getAnnoImmatricolazione() {
        return annoImmatricolazione;
    }

    public void setAnnoImmatricolazione(int annoImmatricolazione) {
        this.annoImmatricolazione = annoImmatricolazione;
    }

    public List<String> getListaCorsi() {
        return listaCorsi;
    }

    public void setListaCorsi(List<String> listaCorsi) {
        this.listaCorsi = listaCorsi;
    }

    //Costruttore
    public Studente(String nome, String cognome, int numeroMatricola, int annoImmatricolazione ){
        this.nome=nome;
        this.cognome=cognome;
        this.numeroMatricola=numeroMatricola;
        this.annoImmatricolazione=annoImmatricolazione;
        List<String> listaCorsi=new ArrayList<>();
        this.listaCorsi=listaCorsi;
    }

    //Metodo iscrizione Corso
    public void iscrizioneCorso(String newCorso){
        listaCorsi.add(newCorso);
    }
    //Metodo rimozioneCorso
    public void rimozioneScorso(String newCorso){
        int indice=listaCorsi.indexOf(newCorso);
        listaCorsi.remove(indice);
    }
}
