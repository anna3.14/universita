import java.util.ArrayList;
import java.util.List;

public class Uni {

    private List<Studente> listaStudenti;
    private List<Professore> listaProf;
    private List<Corso> listaCorsi;

    public List<Studente> getListaStudenti() {
        return listaStudenti;
    }

    public void setListaStudenti(List<Studente> listaStudenti) {
        this.listaStudenti = listaStudenti;
    }

    public List<Professore> getListaProf() {
        return listaProf;
    }

    public void setListaProf(List<Professore> listaProf) {
        this.listaProf = listaProf;
    }

    public List<Corso> getListaCorsi() {
        return listaCorsi;
    }

    public void setListaCorsi(List<Corso> listaCorsi) {
        this.listaCorsi = listaCorsi;
    }

    //Costruttore
    public Uni(){
        List<Studente> listaStudenti=new ArrayList<>();
        this.listaStudenti=listaStudenti;
        List<Professore> listaProf=new ArrayList<>();
        this.listaProf=listaProf;
        List<Corso> listaCorsi=new ArrayList<>();
        this.listaCorsi=listaCorsi;
    }

    //Registrare studente o professore
    public void registrazioneStudente(Studente studente) {
        listaStudenti.add(studente);

    }
    public void registrazioneProfessore(Professore professore) {
        listaProf.add(professore);
    }

    //Creazione di un nuovo corso
    public void registrazioneCorso(Corso corso){
        listaCorsi.add(corso);
    }

    //Iscrizione studente a corso
    public void registrazioneStudenteCorso(Corso corso, Studente studente) throws Exception {
        corso.aggiuntaStudente(studente);
        if(corso.getStudentiIscritti().size()>corso.getMaxStudenti()){
            throw new Exception("Raggiunto numero iscritti max");
        }
        studente.iscrizioneCorso(corso.getTitoloCorso());
    }

    //Assegnazione di un professore a un corso.
    public void registrazioneProfCorso(Corso corso, Professore prof){
        corso.assegnaProf(prof.getNome());
        prof.iscrizioneCorso(corso.getTitoloCorso());

    }

    //Metodo per stampare l'elenco degli studenti in un dato corso
    public void elencoStudentiCorso(Corso corso){
        List<Studente> lista=corso.getStudentiIscritti();
        for (int i=0;i<lista.size();i++){
            Studente studente=lista.get(i);
            System.out.println("Nome: "+studente.getNome());
            System.out.println("Cognome: "+studente.getCognome());
            System.out.println("ID: "+studente.getNumeroMatricola());

        }
    }

    public void elencoCorsiProfessore(Professore prof ){
        List<String> lista=prof.getListaCorsiInsegnati();
        for (int i=0;i<lista.size();i++){
            String elemento=lista.get(i);
            System.out.println("Nome Corso: "+elemento);

        }
    }
    //Ricerca professore
    public void ricercaProf(Professore prof) {
        List<String> lista=prof.getListaCorsiInsegnati();
                System.out.println("Nome Prof: " + prof.getNome());
                System.out.println("Cognome Prof: " + prof.getCognome());
                System.out.println("Codice Prof: " + prof.getID());
                System.out.println("Lista insegnamenti: " + lista);
    }
    //Ricerca studente
    public void ricercaStudente(Studente studente) {
        List<String> listaCorsi=studente.getListaCorsi();
        System.out.println("Nome studente: " + studente.getNome());
        System.out.println("Cognome studente: " + studente.getCognome());
        System.out.println("Numero matricola: " + studente.getNumeroMatricola());
        System.out.println("Lista corsi studente: " + listaCorsi);
    }
    //Ricerca per corso
    public void ricercaCorso(Corso corso) {
        List<Studente> lista=corso.getStudentiIscritti();
        System.out.println("Nome Corso: " + corso.getTitoloCorso());
        System.out.println("Prof Corso: " + corso.getProfessoreTitolare());
        System.out.println("Codice Corso: " + corso.getCodiceCors());
        System.out.println("Iscritti: ");
        for(Studente studente : lista){
            System.out.print(studente.getNome()+" "+studente.getCognome()+" ");
        }
    }


}
