import java.util.ArrayList;
import java.util.List;

public class Professore {
    private String nome;
    private String cognome;
    private int ID;

    private List<String> listaCorsiInsegnati;


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public List<String> getListaCorsiInsegnati() {
        return listaCorsiInsegnati;
    }

    public void setListaCorsiInsegnati(List<String> listaCorsiInsegnati) {
        this.listaCorsiInsegnati = listaCorsiInsegnati;
    }

    //Costruttore
    public Professore(String nome, String cognome, int ID){
        this.nome=nome;
        this.cognome=cognome;
        this.ID=ID;
        List<String> listaCorsiInsegnati=new ArrayList<>();
        this.listaCorsiInsegnati=listaCorsiInsegnati;
    }

    //Metodo iscrizione Corso
    public void iscrizioneCorso(String newCorso){
        listaCorsiInsegnati.add(newCorso);
    }
    //Metodo rimozioneCorso
    public void rimozioneScorso(String newCorso){
        int indice=listaCorsiInsegnati.indexOf(newCorso);
        listaCorsiInsegnati.remove(indice);
    }
}
