
public class Main {
    public static void main(String[] args) throws Exception {
        Uni bicocca=new Uni();
        Studente anna=new Studente("Anna","Crippa",841189,2019);
        Studente sofia=new Studente("Sofia","Di Blasi",821169,2017);

        Studente lorenzo=new Studente("Lorenzo","Abate",981177,2019);
        Professore prof1=new Professore("Gennaro","DeCristofano",32456);
        Corso algebra=new Corso("Algebra","DeCristofano",4231,100);
        Corso biofotonica=new Corso("Biofotonica","Sironi",5781,50);

        bicocca.registrazioneProfCorso(algebra,prof1);
        bicocca.registrazioneProfCorso(biofotonica,prof1);

        try{
        bicocca.registrazioneStudenteCorso(algebra,lorenzo);
        bicocca.registrazioneStudenteCorso(algebra,anna);
        bicocca.registrazioneStudenteCorso(algebra,sofia);
        } catch(Exception ex){
            System.out.println(ex.getMessage());
        }
        System.out.println("-------------------------------------------------------");
        bicocca.elencoStudentiCorso(algebra);
        System.out.println("-------------------------------------------------------");
        bicocca.elencoCorsiProfessore(prof1);

        System.out.println("--------------------------------------------------------");
        bicocca.ricercaProf(prof1);
        System.out.println("--------------------------------------------------------");
        bicocca.ricercaStudente(lorenzo);
        System.out.println("--------------------------------------------------------");
        bicocca.ricercaCorso(algebra);
    }
}